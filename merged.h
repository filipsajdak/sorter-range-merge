#ifndef MERGED_HPP
#define MERGED_HPP

#include <type_traits>

template <typename T1, typename T2, typename Comparator>
struct merge_iterator
{
	T1 ia, ea;
	T2 ib, eb;
	Comparator compare;

	merge_iterator(T1 ia, T1 ea, T2 ib, T2 eb, Comparator c) : ia{ ia }, ib{ ib }, ea{ ea }, eb{ eb }, compare(c)
	{
	}

	merge_iterator(T1 ea, T2 eb, Comparator c) : merge_iterator(ea, ea, eb, eb, c){}

	merge_iterator& operator++() {
		if (ia != ea && ib != eb)
		{
			if (compare(*ia, *ib)) {
				++ia;
			}
			else if (compare(*ib, *ia)){
				++ib;
			}
			else {
				++ia;
				++ib;
			}
		}
		else if (ia != ea) {
			++ia;
		}
		else {
			++ib;
		}
		return *this;
	}

	auto operator*() const -> decltype(*ia) {
		if (ia != ea && ib != eb)
		{
			if (compare(*ia, *ib)) {
				return *ia;
			}
			else if (compare(*ib, *ia)) {
				return *ib;
			}
			else {
				return *ia;
			}
		}
		else if (ia != ea) {
			return *ia;
		}
		else {
			return *ib;
		}
	}

	bool operator==(const merge_iterator& lhs) const
	{
		return ia == lhs.ia && ib == lhs.ib;
	}

	bool operator!=(const merge_iterator& lhs) const
	{
		return !(*this == lhs);
	}
};

template <typename T1, typename T2, typename Compare = 	typename std::enable_if< std::is_same<typename T1::key_compare, typename T2::key_compare>::value, 
	typename T1::key_compare>::type
>
struct merge_containers
{
	using iterator = merge_iterator < typename T1::iterator, typename T2::iterator, Compare > ;
	using key_compare = Compare;

	typename T1::iterator ia, ea;
	typename T2::iterator ib, eb;
	Compare compare;

	merge_containers(const T1& a, const T2& b, const Compare& c) : ia{ a.begin() }, ea{ a.end() }, ib{ b.begin() }, eb{ b.end() }, compare(c)
	{
	}

	iterator begin() const
	{
		return{ ia, ea, ib, eb, compare };
	}

	iterator end() const
	{
		return{ ea, eb, compare };
	}
};

template <typename T1>
const T1& merged(const T1& a_)
{
	return a_;
}


template <typename T1, typename T2>
merge_containers<T1, T2> merged(const T1& a_, const T2& b_)
{
	static_assert(std::is_same<typename T1::key_compare, typename T2::key_compare>::value, "Merged containers has to have same key_compare!");
	return{ a_, b_, a_.key_comp() };
}

template <typename T, typename... Args>
auto merged(const T& a, const Args&... args) -> decltype(merged(a, merged(args...)))
{
	static_assert(std::is_same<typename T::key_compare, decltype(merged(args...))::key_compare>::value, "Merged containers has to have same key_compare!");
	return merged(a, merged(args...));
}


#endif
